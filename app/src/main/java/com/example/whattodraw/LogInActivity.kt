package com.example.whattodraw

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_favourites.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.emailField
import kotlinx.android.synthetic.main.activity_login.logInButton
import kotlinx.android.synthetic.main.activity_login.passwordField
import kotlinx.android.synthetic.main.activity_login.registrationButton
import kotlinx.android.synthetic.main.activity_registration.*

class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }


    private fun init() {
        readData()
        auth = FirebaseAuth.getInstance()
        logInButton.setOnClickListener {
            logIn()
            saveData()
        }

        registrationButton.setOnClickListener {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
        }

    }

    private fun logIn() {
        if (emailField.text.isEmpty() || passwordField.text.isEmpty()) {
            Toast.makeText(this, "შეავსეთ ყველა მინდორი", Toast.LENGTH_SHORT).show()
        } else {
            auth.signInWithEmailAndPassword(
                emailField.text.toString(),
                passwordField.text.toString()
            )
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Log.d("log in", "signInWithEmail:success")
                        val user = auth.currentUser
                        val intent = Intent(this, FavouritesActivity::class.java)

            intent.putExtra("email", emailField.text.toString())
            intent.putExtra("password", passwordField.text.toString())
            startActivity(intent)
                    } else {

                        Log.w("log in", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "უპს, რაღაც შეცდომაა!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }

    private fun saveData() {
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email", emailField.text.toString())
        edit.putString("password", passwordField.text.toString())
        edit.commit()

    }

    private fun readData() {
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        emailField.setText(sharedPreferences.getString("email", ""))
        passwordField.setText(sharedPreferences.getString("password", ""))

    }
}

