package com.example.whattodraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_character.*

class NatureActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nature)
        init()
    }

    private fun init(){
        val timeArray10 = listOf("განთიადი","დილა","მზის ამოსვლა","შუადღე","დღე","შებინდება",
            "მწუხრი","მზის ჩასვლა","საღამო","ღამე","შუაღამე")
        val moodArray10 = listOf("მელანქოლიურობა","სიმშვიდე","დაღლილობა","ჰარმონიულობა",
            "იდილია","ბედნიერება","სიხარული","სევდიანობა","ამაღელვებელი","აღტაცებადი","სიყვარულით სავსე")
        val locationArray20 = listOf("სტადიონი", "ეზო", "პარკი", "ტყე", "ბილიკი",
            "ნავი", "ზღვა", "მთა", "სოფელი", "მდინარე","ტბა","უდაბნო","კურორტი","შადრევანი","ჩანჩქერი",
            "პიკნიკი","კუნძული","ოაზისი","მინდორი","ბალახი","ზოოპარკი")
        val weatherArray20 = listOf("მზიანი", "მოღრუბლული", "წვიმიანი", "ქარიანი", "ყინავს", "სიცხიანი",
            "სიგრილეა", "სითბოა", "ზამთრის", "გაზაფხულის", "შემოდგომის", "ზაფხულის", "თოვს", "სეტყვიანი",
            "ჭექა-ქუხილიანი", "პაპანაქება სიცხეა", "ზომიერი", "კოკისპირულად წვიმს", "ცრის", "გვალვიანი", "სუსხიანი"
        )


        generateBtn.setOnClickListener {
            val randomIndex10 = (0..10).random()
            val randomIndex20 = (0..20).random()

            btn1.text = moodArray10.get(randomIndex10)
            btn2.text = timeArray10.get(randomIndex10)
            btn3.text = locationArray20.get(randomIndex20)
            btn4.text = weatherArray20.get(randomIndex20)
            fullSentenceTextView.text = "განწყობა: " + moodArray10.get(randomIndex10) + ", დრო: " + timeArray10.get(randomIndex10) + ", ლოკაცია: " + locationArray20.get(randomIndex20) + ", ამინდი: " + weatherArray20.get(randomIndex20)
        }


        btn1.setOnClickListener {
            val randomIndex10 = (0..10).random()

            btn1.text = moodArray10.get(randomIndex10)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "განწყობა: " + moodArray10.get(randomIndex10) + ", დრო: " + btn2.text + ", ლოკაცია: " + btn3.text + ", ამინდი: " + btn4.text
            }
        }
        btn2.setOnClickListener {
            val randomIndex10 = (0..10).random()
            btn2.text = timeArray10.get(randomIndex10)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "განწყობა: " + btn1.text + ", დრო: " + timeArray10.get(randomIndex10) + ", ლოკაცია: " + btn3.text + ", ამინდი: " +btn4.text
            }
        }
        btn3.setOnClickListener {
            val randomIndex20 = (0..20).random()
            btn3.text = locationArray20.get(randomIndex20)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "განწყობა: " + btn1.text+ ", დრო: " + btn2.text + ", ლოკაცია: " + locationArray20.get(randomIndex20) + ", ამინდი: " + btn4.text
            }
        }
        btn4.setOnClickListener {
            val randomIndex20 = (0..20).random()
            btn4.text =weatherArray20.get(randomIndex20)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text =  "განწყობა: " + btn1.text + ", დრო: " + btn2.text+ ", ლოკაცია: " + btn3.text + ", ამინდი: " + weatherArray20.get(randomIndex20)
            }

        }

        choiceBtn.setOnClickListener {
            if (fullSentenceTextView.text.isEmpty()) {
                Toast.makeText(this, "ჯერ არ შედგენილა წინადადება", Toast.LENGTH_SHORT).show()
            }
//            else if(fav1.text.isNotEmpty() and fav2.text.isNotEmpty() and fav3.text.isNotEmpty() and fav4.text.isNotEmpty() and fav5.text.isNotEmpty() and fav6.text.isNotEmpty() and fav7.text.isNotEmpty() and fav8.text.isNotEmpty() and fav9.text.isNotEmpty() and fav10.text.isNotEmpty()) {
//                Toast.makeText(this, "სამწუხაროდ ლიმიტი ამოწურულია", Toast.LENGTH_SHORT).show()
//            }
            else {
                val intent = Intent(this, FavouritesActivity::class.java)
                Toast.makeText(this, "წარმატებით დაემატა არჩეულთა სიას!", Toast.LENGTH_SHORT).show()
                var favourite = fullSentenceTextView.text.toString()
                intent.putExtra("favourite", favourite)
                startActivity(intent)
            }
        }

    }

}
