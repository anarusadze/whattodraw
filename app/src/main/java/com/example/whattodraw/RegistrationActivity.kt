package com.example.whattodraw

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.emailField
import kotlinx.android.synthetic.main.activity_login.passwordField
import kotlinx.android.synthetic.main.activity_login.registrationButton
import kotlinx.android.synthetic.main.activity_registration.*
import kotlin.math.sign

class RegistrationActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        signUp()
        logIn()
    }

    private fun signUp() {

        auth = FirebaseAuth.getInstance()
        registrationButton.setOnClickListener {
            if (emailField.text.isEmpty() || passwordField.text.isEmpty()) {
                Toast.makeText(this, "შეავსეთ ყველა მინდორი", Toast.LENGTH_SHORT).show()
            } else {
//                if (passwordField.text != repeatPasswordField.text) {
//                    Toast.makeText(baseContext, "პაროლები განსხვავებულია!", Toast.LENGTH_SHORT)
//                        .show()
//                } else {
                    auth.createUserWithEmailAndPassword(emailField.text.toString(), passwordField.text.toString()).addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Log.d("SignUp", "createUserWithEmail:success")
                            Toast.makeText(baseContext, "წარმატებული რეგისტრაცია!", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, LogInActivity::class.java)
                            startActivity(intent)

                        } else {
                            Log.w("SignUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "უპს, რაღაც შეცდომაა", Toast.LENGTH_SHORT).show()
                        }

                    }
                }

            }
        }




    private fun logIn(){
        logInButton.setOnClickListener {
            val intent = Intent(this, LogInActivity::class.java)
            startActivity(intent)
        }
    }
}
