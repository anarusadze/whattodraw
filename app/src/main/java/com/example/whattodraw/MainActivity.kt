package com.example.whattodraw

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        defaultTextView.setOnClickListener{
            val alert = AlertDialog.Builder(this)
            alert.setTitle("რეგისტრაცია")
            alert.setMessage("გაიარეთ რეგისტრაცია, რათა იხილოთ არჩეულთა სია")
            alert.setPositiveButton(
                "რეგისტრაცია",
                {dialogInterface: DialogInterface, i: Int -> startActivity(Intent(
                            this, RegistrationActivity::class.java)) })
            alert.setNegativeButton("არ მინდა", { dialogInterface: DialogInterface, i: Int -> })
            alert.show() }

        characterTextView.setOnClickListener{
            val intent = Intent(this, CharacterActivity::class.java)
            startActivity(intent)
        }
        natureTextView.setOnClickListener{
            val intent = Intent(this, NatureActivity::class.java)
            startActivity(intent)
        }
        animalTextView.setOnClickListener{
            val intent = Intent(this, AnimalActivity::class.java)
            startActivity(intent)
        }
        mysticTextView.setOnClickListener{
            val intent = Intent(this, SuperheroActivity::class.java)
            startActivity(intent)
        }
        naturmortTextView.setOnClickListener{
            val intent = Intent(this, NaturmortActivity::class.java)
            startActivity(intent)
        }
    }
}
