package com.example.whattodraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_character.*
import kotlinx.android.synthetic.main.activity_favourites.*

class
CharacterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)
        init()
    }

    private fun init() {
        val verbArray50 = listOf(
            "იცინის",
            "ტირის",
            "იცვამს",
            "ხატავს",
            "ალაგებს სახლს",
            "ხტუნაობს",
            "ვარჯიშობს",
            "რეცხავს ჭურჭელს",
            "უყურებს ტელევიზორს",
            "თამაშობს",
            "მეცადინეობს",
            "მიდის სკოლაში",
            "მიდის სამსახურში",
            "მუშაობს",
            "სვამს",
            "ჭამს",
            "ივარცხნის თმას",
            "ცეკვავს",
            "იპრანჭება",
            "იძინებს",
            "ბანაობს",
            "ჩემოდანს ალაგებს",
            "მოგზაურობს",
            "სურათებს ათვალიერებს",
            "აცხობს",
            "აუთოებს",
            "სარეცხს ფენს",
            "ოცნებობს",
            "ფიქრობს",
            "კითხულობს",
            "წერს",
            "სწავლობს",
            "წევს",
            "ფუთავს საჩუქარს",
            "ყიდულობს საჭმელს",
            "სრიალებს",
            "სეირნობს",
            "გემრიელად ილუკმება",
            "ირუჯება",
            "კბილებს იხეხავს",
            "დგას რიგში",
            "ატარებს მანქანას",
            "ველოსიპედს ატარებს",
            "იშუშებს ნატკენს",
            "ავად არის",
            "რეკავს ტელეფონზე",
            "აგზავნის სასიყვარულო წერილებს",
            "უკრავს ინსტრუმენტზე",
            "უყურებს ფეხბურთს",
            "მღერის",
            "ასწორებს ლოგინს"
        )
        val characteristicArray50 = listOf(
            "ბედნიერი",
            "გახარებული",
            "აღტაცებული",
            "სასოწარკვეთილი",
            "სევდიანი",
            "უიმედო",
            "გაბრაზებული",
            "გაკვირვებული",
            "დაბნეული",
            "მოვლილი",
            "უბედური",
            "ნაწყენი",
            "აღფრთოვანებული",
            "სარკასტული",
            "ზარმაცი",
            "ამაყი",
            "სერიოზული",
            "ბავშვური",
            "სიყვარულით სავსე",
            "შეუხედავი",
            "გაბუტული",
            "მოწყენილი",
            "ჭკვიანი",
            "ღორმუცელა",
            "მოწესრიგებული",
            "მარტოხელა",
            "დაკუნთული",
            "მეოცნებე",
            "მაღალი",
            "დაბალი",
            "მსუქანი",
            "გამხდარი",
            "ეჭვიანი",
            "მოსიყვარულე",
            "ქალთმოძულე",
            "მდიდარი",
            "კეთილი",
            "ბოროტი",
            "ძუნწი",
            "ნერვიული",
            "ამპარტავანი",
            "მოუწესრიგებელი",
            "სულელი",
            "საეჭვო გარეგნობის",
            "თბილად ჩაცმული",
            "გულგრილი",
            "ბინძური",
            "გამოწყობილი",
            "აფერისტი",
            "მორცხვი",
            "თამამი"
        )
        val professionArray50 = listOf(
            "ბუღალტერი",
            "მცხობელი",
            "ბანკირი",
            "ბიბლიოთეკარი",
            "სუმოისტი",
            "პროგრამისტი",
            "ფეხბურთელი",
            "ხელოსანი",
            "მუშა",
            "კომპოზიტორი",
            "მეპურე",
            "მძღოლი",
            "გამყიდველი",
            "მევახშე",
            "ვაჭარი",
            "მეფე",
            "ვეზირი",
            "გამომძიებელი",
            "მკერავი",
            "წამყვანი",
            "მსახიობი",
            "მოცეკვავე",
            "მომღერალი",
            "ჟურნალისტი",
            "ოპერატორი",
            "რეჟისორი",
            "დირექტორი",
            "მასწავლებელი",
            "მანდატური",
            "ექიმი",
            "სტომატოლოგი",
            "პედიატრი",
            "ქირურგი",
            "ფსიქოლოგი",
            "ფარმაცეფტი",
            "პრეზიდენტი",
            "პოლიტიკოსი",
            "მინისტრი",
            "მრეცხავი",
            "მდივანი",
            "ბალერინა",
            "დეტექტივი",
            "პოლიციელი",
            "ადვოკატი",
            "პროკურორი",
            "მოსამართლე",
            "მზარეული",
            "ოფიციანტი",
            "დამლაგებელი",
            "ბიზნესმენი",
            "ფერმერი"
        )
        val addElementArray50 = listOf(
            "ყვავილი",
            "თაფლი",
            "რძე",
            "თეფში",
            "სათამაშო",
            "ბალიში",
            "ტელევიზორი",
            "ჯამი",
            "ჭიქა",
            "მცენარე",
            "წინდები",
            "საათი",
            "ნაყინი",
            "მაცივარი",
            "სოსისი",
            "ქვაბი",
            "ავეჯი",
            "მაღაზია",
            "სახლი",
            "ტანსაცმელი",
            "ჟაკეტი",
            "მაისური",
            "გასაღები",
            "კარი",
            "ლეპტოპი",
            "მანქანა",
            "რვეული",
            "ჩარჩო",
            "ცარცი",
            "კომპიუტერი",
            "ფურცელი",
            "ხიდი",
            "პატრული",
            "პიანინო",
            "გიტარა",
            "ნესვი",
            "მსხალი",
            "ცეზარი",
            "ხილის ასორტი",
            "ბანტი",
            "ნაძვის ხე",
            "ჩიჩილაკი",
            "საფულე",
            "ნათურა",
            "კოსტიუმი",
            "ტელეფონი",
            "ლეპტოპი",
            "მანდარინი",
            "ღვინო",
            "სამკაული",
            "დამტენი"
        )


        generateBtn.setOnClickListener {
            val randomIndex50 = (0..50).random()
            btn1.text = characteristicArray50.get(randomIndex50)
            btn2.text = professionArray50.get(randomIndex50)
            btn3.text = verbArray50.get(randomIndex50)
            btn4.text = addElementArray50.get(randomIndex50)
            fullSentenceTextView.text = characteristicArray50.get(randomIndex50) + " " + professionArray50.get(randomIndex50) + ", რომელიც " + verbArray50.get(randomIndex50) + ". დამატებითი ელემენტი: " + addElementArray50.get(randomIndex50)
        }


        btn1.setOnClickListener {
            val randomIndex50 = (0..50).random()
            btn1.text = characteristicArray50.get(randomIndex50)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = characteristicArray50.get(randomIndex50) + " " + btn2.text + ", რომელიც " + btn3.text + ". დამატებითი ელემენტი: " + btn4.text
            }
        }
        btn2.setOnClickListener {
            val randomIndex50 = (0..50).random()
            btn2.text = professionArray50.get(randomIndex50)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "" + btn1.text + " " + professionArray50.get(randomIndex50) + ", რომელიც " + btn3.text + ". დამატებითი ელემენტი: " + btn4.text
            }
        }
        btn3.setOnClickListener {
            val randomIndex50 = (0..20).random()
            btn3.text = verbArray50.get(randomIndex50)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "" + btn1.text + " " + btn2.text + ", რომელიც " + verbArray50.get(randomIndex50) + ". დამატებითი ელემენტი: " + btn4.text
            }
        }
        btn4.setOnClickListener {
            val randomIndex50 = (0..50).random()
            btn4.text = addElementArray50.get(randomIndex50)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "" + btn1.text + " " + btn2.text + ", რომელიც " + btn3.text + ". დამატებითი ელემენტი: " + addElementArray50.get(randomIndex50)
            }

        }
        choiceBtn.setOnClickListener {
            if (fullSentenceTextView.text.isEmpty()) {
                Toast.makeText(this, "ჯერ არ შედგენილა წინადადება", Toast.LENGTH_SHORT).show()
            }
//            else if(fav1.text.isNotEmpty() and fav2.text.isNotEmpty() and fav3.text.isNotEmpty() and fav4.text.isNotEmpty() and fav5.text.isNotEmpty() and fav6.text.isNotEmpty() and fav7.text.isNotEmpty() and fav8.text.isNotEmpty() and fav9.text.isNotEmpty() and fav10.text.isNotEmpty()) {
//                Toast.makeText(this, "სამწუხაროდ ლიმიტი ამოწურულია", Toast.LENGTH_SHORT).show()
//            }
            else {
                val intent = Intent(this, FavouritesActivity::class.java)
                Toast.makeText(this, "წარმატებით დაემატა არჩეულთა სიას!", Toast.LENGTH_SHORT).show()
                var favourite = fullSentenceTextView.text.toString()
                intent.putExtra("favourite", favourite)
                startActivity(intent)
            }
        }
    }
}


