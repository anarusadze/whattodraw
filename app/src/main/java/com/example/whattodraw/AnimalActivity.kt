package com.example.whattodraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_character.*

class AnimalActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_animal)
        init()
    }

    private fun init() {
        val animalArray50 = listOf(
            "სპილო", "ჟირაფი", "კატა", "კენგურუ", "დათვი", "ძაღლი", "მგელი",
            "მელია", "მერცხალი", "ბუ", "ჭოტი", "ზღარბი", "ხოჭო", "დელფინი",
            "ვეშაპი", "ბეგემოტი", "მარტორქა", "კურდღელი", "კუ", "ლეოპარდი", "ვეფხვი", "ლომი",
            "გეპარდი", "ირემი", "ანტილოპა", "თევზი", "ბაყაყი", "რწყილი", "ჭიანჭველა",
            "ხვლიკი", "კოდალა", "პინგვინი", "ტურა", "ქათამი", "ბატი", "იხვი", "სირაქლემა", "კოალა",
            "ენოტი", "თეთრი დათვი", "ზაზუნა", "ლოსი", "დინოზავრი", "თაგვი", "ვირთხა", "გველი",
            "ნიანგი", "თხუნელა", "თუთიყუში", "აქლემი", "ზებრა"
        )
        val colorArray20 = listOf(
            "შავი",
            "ნაცრისფერი",
            "ვერცხლისფერი",
            "თეთრი",
            "ოქროსფერი",
            "ყავისფერი",
            "ჭაობისფერი",
            "ფირუზისფერი",
            "ვარდისფერი",
            "შინდისფერი",
            "ალისფერი",
            "ბორდოსფერი",
            "ალუბლისფერი",
            "ხაკისფერი",
            "წითელი",
            "ნარინჯისფერი",
            "ყვითელი",
            "მწვანე",
            "ცისფერი",
            "ლურჯი",
            "იისფერი"
        )
        val locationArray50 = listOf(
            "სკოლა",
            "სახლი",
            "სამსახური",
            "ლოგინი",
            "სამზარეულო",
            "აბაზანა",
            "სადარბაზო",
            "ბანკი",
            "რესტორანი",
            "კაფე",
            "ბარი",
            "სტადიონი",
            "ეზო",
            "პარკი",
            "ქუჩა",
            "ტყე",
            "ბილიკი",
            "დარბაზი",
            "თეატრი",
            "კინოთეატრი",
            "მაკდონალდსი",
            "სანაყინე",
            "ქარხანა",
            "თვითმფრინავი",
            "მანქანა",
            "გემი",
            "ნავი",
            "კლუბი",
            "ავტობუსი",
            "მაღაზია",
            "ქიმწმენდა",
            "სახელოსნო",
            "სტუმრად",
            "სალონი",
            "გასაუბრება",
            "ზღვა",
            "მთა",
            "საზღვარგარეთ",
            "აეროპორტი",
            "საზღვაო პორტი",
            "კონცერტი",
            "ქორწილი",
            "პანაშვიდი",
            "ზოოპარკი",
            "ატრაქციონი",
            "აუზი",
            "სუფრა",
            "სოფელი",
            "ბაზარი",
            "სავაჭრო ცენტრი",
            "სიცილის ოთახი"
        )
        val weatherArray20 = listOf(
            "მზიანი",
            "მოღრუბლული",
            "წვიმიანი",
            "ქარიანი",
            "ყინავს",
            "სიცხიანი",
            "სიგრილეა",
            "სითბოა",
            "ზამთრის",
            "გაზაფხულის",
            "შემოდგომის",
            "ზაფხულის",
            "თოვს",
            "სეტყვიანი",
            "ჭექა-ქუხილიანი",
            "პაპანაქება სიცხეა",
            "ზომიერი",
            "კოკისპირულად წვიმს",
            "ცრის",
            "გვალვიანი",
            "სუსხიანი"
        )



        generateBtn.setOnClickListener {
            val randomIndex50 = (0..50).random()
            val randomIndex20 = (0..20).random()

            btn1.text = colorArray20.get(randomIndex20)
            btn2.text = animalArray50.get(randomIndex50)
            btn3.text = locationArray50.get(randomIndex50)
            btn4.text = weatherArray20.get(randomIndex20)
            fullSentenceTextView.text =
                colorArray20.get(randomIndex20) + " " + animalArray50.get(randomIndex50) + ", ლოკაცია: " + locationArray50.get(
                    randomIndex50
                ) + ", ამინდი: " + weatherArray20.get(randomIndex20)
        }


        btn1.setOnClickListener {
            val randomIndex20 = (0..20).random()
            btn1.text = colorArray20.get(randomIndex20)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text =
                    colorArray20.get(randomIndex20) + " " + btn2.text + ", ლოკაცია: " + btn3.text + ", ამინდი: " + btn4.text
            }
        }
        btn2.setOnClickListener {
            val randomIndex50 = (0..50).random()
            btn2.text = animalArray50.get(randomIndex50)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text =
                    "" + btn1.text + " " + animalArray50.get(randomIndex50) + ", ლოკაცია: " + btn3.text + ", ამინდი: " + btn4.text

            }
        }
            btn3.setOnClickListener {
                val randomIndex50 = (0..20).random()
                btn3.text = locationArray50.get(randomIndex50)
                if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                    fullSentenceTextView.text =
                        "" + btn1.text + " " + btn2.text + ", ლოკაცია: " + locationArray50.get(
                            randomIndex50
                        ) + ", ამინდი: " + btn4.text
                }
            }
            btn4.setOnClickListener {
                val randomIndex20 = (0..20).random()
                btn4.text = weatherArray20.get(randomIndex20)
                if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                    fullSentenceTextView.text =
                        "" + btn1.text + " " + btn2.text + ", ლოკაცია: " + btn3.text + ", ამინდი: " + weatherArray20.get(
                            randomIndex20
                        )
                }

            }

        choiceBtn.setOnClickListener {
            if (fullSentenceTextView.text.isEmpty()) {
                Toast.makeText(this, "ჯერ არ შედგენილა წინადადება", Toast.LENGTH_SHORT).show()
            }
//            else if(fav1.text.isNotEmpty() and fav2.text.isNotEmpty() and fav3.text.isNotEmpty() and fav4.text.isNotEmpty() and fav5.text.isNotEmpty() and fav6.text.isNotEmpty() and fav7.text.isNotEmpty() and fav8.text.isNotEmpty() and fav9.text.isNotEmpty() and fav10.text.isNotEmpty()) {
//                Toast.makeText(this, "სამწუხაროდ ლიმიტი ამოწურულია", Toast.LENGTH_SHORT).show()
//            }
            else {
                val intent = Intent(this, FavouritesActivity::class.java)
                Toast.makeText(this, "წარმატებით დაემატა არჩეულთა სიას!", Toast.LENGTH_SHORT).show()
                var favourite = fullSentenceTextView.text.toString()
                intent.putExtra("favourite", favourite)
                startActivity(intent)
            }
        }


    }
}

