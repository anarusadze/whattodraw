package com.example.whattodraw

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_character.*

class SuperheroActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_superhero)
        init()
    }

    private fun init() {
        val superPowerArray20 = listOf(
            "ტელეპორტაცია",
            "გაუჩინარება",
            "სიმძიმეების აწევა",
            "სწრაფად სირბილი",
            "ჯადოქრობა",
            "დროში მოგზაურობა",
            "ადამიანებად გადაქცევა",
            "რენტგენურად დანახვა",
            "მომავლის განჭვრეტა",
            "ფიქრების კითხვა",
            "ქალთევზად გადაქცევა",
            "რეზინასავით გაწელვა",
            "წყალში სუნთქვა",
            "წყლის მართვა",
            "მიწის მართვა",
            "ცეცხლის მართვა",
            "ჰაერის მართვა",
            "მეხსიერების წაშლა",
            "ფრენა",
            "მეტალის მართვა",
            "მანიპულაცია"
        )
        val verbArray50 = listOf(
            "იცინის",
            "ტირის",
            "კოცნის შვილს",
            "ხატავს",
            "ალაგებს სახლს",
            "ხტუნაობს",
            "ვარჯიშობს",
            "რეცხავს ჭურჭელს",
            "უყურებს ტელევიზორს",
            "თამაშობს",
            "მეცადინეობს",
            "მიდის სკოლაში",
            "მიდის სამსახურში",
            "მუშაობს",
            "სვამს",
            "ჭამს",
            "ერთობა",
            "ცეკვავს",
            "იპრანჭება",
            "იძინებს",
            "ბანაობს",
            "ჩემოდანს ალაგებს",
            "მოგზაურობს",
            "სურათებს ათვალიერებს",
            "აცხობს",
            "აუთოებს",
            "სარეცხს ფენს",
            "ოცნებობს",
            "ფიქრობს",
            "კითხულობს",
            "წერს",
            "სწავლობს",
            "წევს",
            "ფუთავს საჩუქარს",
            "ყიდულობს საჭმელს",
            "სრიალებს",
            "სეირნობს",
            "გემრიელად ილუკმება",
            "ირუჯება",
            "კბილებს იხეხავს",
            "რიგში დგას",
            "ატარებს მანქანას",
            "ველოსიპედს ატარებს",
            "იშუშებს ნატკენს",
            "ავად არის",
            "რეკავს ტელეფონზე",
            "აგზავნის სასიყვარულო წერილებს",
            "უკრავს ინსტრუმენტზე",
            "უყურებს ფეხბურთს",
            "მღერის",
            "ასწორებს ლოგინს"
        )
        val addElementArray20 = listOf(
            "მოსასხამი", "ჯოხი", "იარაღი", "სათვალე", "დურბინტი", "შინაური ცხოველი",
            "წიგნი", "საათი", "ჯადოსნური ქუდი", "ზურგჩანთა", "ლეპტოპი", "ბომბი",
            "მაგიური სასმელი", "ჩემოდანი", "ტექნო-გამოგონება", "მაგნიტი", "ნიღაბი", "რაკეტა",
            "ცოცხი", "ჩექმები", "ჩაპხუტი"
        )
        val colorArray20 = listOf(
            "შავი",
            "ნაცრისფერი",
            "ვერცხლისფერი",
            "თეთრი",
            "ოქროსფერი",
            "ყავისფერი",
            "ჭაობისფერი",
            "ფირუზისფერი",
            "ვარდისფერი",
            "შინდისფერი",
            "ალისფერი",
            "ბორდოსფერი",
            "ალუბლისფერი",
            "ხაკისფერი",
            "წითელი",
            "ნარინჯისფერი",
            "ყვითელი",
            "მწვანე",
            "ცისფერი",
            "ლურჯი",
            "იისფერი"
        )


        generateBtn.setOnClickListener {
            val randomIndex20 = (0..20).random()
            val randomIndex50 = (0..50).random()
            btn1.text = superPowerArray20.get(randomIndex20)
            btn2.text = colorArray20.get(randomIndex20)
            btn3.text = verbArray50.get(randomIndex50)
            btn4.text = addElementArray20.get(randomIndex20)
            fullSentenceTextView.text = "სუპერგმირი " + colorArray20.get(randomIndex20) + " ფორმით, რომელსაც შეუძლია: " + superPowerArray20.get(randomIndex20) + ", " + verbArray50.get(randomIndex50) + ". დამატებითი ელემენტი: " + addElementArray20.get(randomIndex20)
        }


        btn1.setOnClickListener {
            val randomIndex20 = (0..20).random()
            btn1.text = superPowerArray20.get(randomIndex20)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text = "სუპერგმირი " + btn2.text + " ფორმით, რომელსაც შეუძლია: " + superPowerArray20.get(randomIndex20) + ", " + btn3.text + ". დამატებითი ელემენტი: " + btn4.text
            }
        }
        btn2.setOnClickListener {
            val randomIndex20 = (0..20).random()
            btn2.text = colorArray20.get(randomIndex20)
            if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                fullSentenceTextView.text =
                    "სუპერგმირი " + colorArray20.get(randomIndex20) + " ფორმით, რომელსაც შეუძლია: " + btn1.text + ", " + btn3.text + ". დამატებითი ელემენტი: " + btn4.text
            }
        }
            btn3.setOnClickListener {
                val randomIndex50 = (0..50).random()
                btn3.text = verbArray50.get(randomIndex50)
                if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                    fullSentenceTextView.text =
                        "სუპერგმირი " + btn2.text + " ფორმით, რომელსაც შეუძლია: " + btn1.text + ", " + verbArray50.get(
                            randomIndex50
                        ) + ". დამატებითი ელემენტი: " + btn4.text
                }
            }
            btn4.setOnClickListener {
                val randomIndex20 = (0..20).random()
                btn4.text = addElementArray20.get(randomIndex20)
                if (btn1.text.isNotEmpty() and btn2.text.isNotEmpty() and btn3.text.isNotEmpty() and btn4.text.isNotEmpty()) {
                    fullSentenceTextView.text =
                        "სუპერგმირი " + btn2.text + " ფორმით, რომელსაც შეუძლია: " + btn1.text + ", " + btn3.text + ". დამატებითი ელემენტი: " + addElementArray20.get(
                            randomIndex20
                        )
                }
            }

        choiceBtn.setOnClickListener {
            if (fullSentenceTextView.text.isEmpty()) {
                Toast.makeText(this, "ჯერ არ შედგენილა წინადადება", Toast.LENGTH_SHORT).show()
            }
//            else if(fav1.text.isNotEmpty() and fav2.text.isNotEmpty() and fav3.text.isNotEmpty() and fav4.text.isNotEmpty() and fav5.text.isNotEmpty() and fav6.text.isNotEmpty() and fav7.text.isNotEmpty() and fav8.text.isNotEmpty() and fav9.text.isNotEmpty() and fav10.text.isNotEmpty()) {
//                Toast.makeText(this, "სამწუხაროდ ლიმიტი ამოწურულია", Toast.LENGTH_SHORT).show()
//            }
            else {
                val intent = Intent(this, FavouritesActivity::class.java)
                Toast.makeText(this, "წარმატებით დაემატა არჩეულთა სიას!", Toast.LENGTH_SHORT).show()
                var favourite = fullSentenceTextView.text.toString()
                intent.putExtra("favourite", favourite)
                startActivity(intent)
            }
        }

        }
    }



