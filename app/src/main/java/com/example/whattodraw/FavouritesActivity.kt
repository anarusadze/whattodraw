package com.example.whattodraw

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_favourites.*


class FavouritesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourites)
        init()
        init2()
        saveData()
        noFavs()

    }


    private fun init() {
        readData()
        val favourites = intent.extras!!.getString("favourite", "")
        if (fav1.text.isEmpty()) {
            fav1.text = favourites.toString()
        } else {
//            nothingTextView.text = ""
            if (fav2.text.isEmpty()) {
                fav2.text = favourites.toString()

            } else {
                if (fav3.text.isEmpty()) {
                    fav3.text = favourites.toString()
                } else {
                    if (fav4.text.isEmpty()) {
                        fav4.text = favourites.toString()
                    } else {
                        if (fav5.text.isEmpty()) {
                            fav5.text = favourites.toString()
                        } else {
                            if (fav6.text.isEmpty()) {
                                fav6.text = favourites.toString()
                            } else {
                                if (fav7.text.isEmpty()) {
                                    fav7.text = favourites.toString()
                                } else {
                                    if (fav8.text.isEmpty()) {
                                        fav8.text = favourites.toString()
                                    } else {
                                        if (fav9.text.isEmpty()) {
                                            fav9.text = favourites.toString()
                                        } else {
                                            if (fav10.text.isEmpty()) {
                                                fav10.text = favourites.toString()
                                            }
                                        }
                                    }
                                }
                            }

//
                        }
                    }

                }
            }
        }

    }


    private fun saveData() {
        val sharedPreferences = getSharedPreferences("favourites", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("favourite1", fav1.text.toString())
        edit.putString("favourite2", fav2.text.toString())
        edit.putString("favourite3", fav3.text.toString())
        edit.putString("favourite4", fav4.text.toString())
        edit.putString("favourite5", fav5.text.toString())
        edit.putString("favourite6", fav6.text.toString())
        edit.putString("favourite7", fav7.text.toString())
        edit.putString("favourite8", fav8.text.toString())
        edit.putString("favourite9", fav9.text.toString())
        edit.putString("favourite10", fav10.text.toString())

        edit.apply()
    }

    private fun readData() {
        val sharedPreferences = getSharedPreferences("favourites", Context.MODE_PRIVATE)
        fav1.text = sharedPreferences.getString("favourite1", "")
        fav2.text = sharedPreferences.getString("favourite2", "")
        fav3.text = sharedPreferences.getString("favourite3", "")
        fav4.text = sharedPreferences.getString("favourite4", "")
        fav5.text = sharedPreferences.getString("favourite5", "")
        fav6.text = sharedPreferences.getString("favourite6", "")
        fav7.text = sharedPreferences.getString("favourite7", "")
        fav8.text = sharedPreferences.getString("favourite8", "")
        fav9.text = sharedPreferences.getString("favourite9", "")
        fav10.text = sharedPreferences.getString("favourite10", "")

    }


    private fun deleteFav(textView: TextView) {
        textView.setOnClickListener {
            if (textView.text.isNotEmpty()) {
                val alert = AlertDialog.Builder(this)
                alert.setTitle("წაშლა")
                alert.setMessage("გსურთ წინადადების წაშლა?")
                alert.setPositiveButton(
                    "დიახ",
                    { dialogInterface: DialogInterface, i: Int ->
//                        textView.text = ""
                        saveDeleted(textView)
                        readDeleted(textView)
                    })
                alert.setNegativeButton("არ მინდა", { dialogInterface: DialogInterface, i: Int ->
                })
                alert.show()
            }
        }
    }


    private fun init2() {
        fav1.setOnClickListener {
            deleteFav(fav1)
        }
        fav2.setOnClickListener {
            deleteFav(fav2)
        }
        fav3.setOnClickListener {
            deleteFav(fav3)
        }
        fav4.setOnClickListener {
            deleteFav(fav4)
        }
        fav5.setOnClickListener {
            deleteFav(fav5)
        }
        fav6.setOnClickListener {
            deleteFav(fav6)
        }
        fav7.setOnClickListener {
            deleteFav(fav7)
        }
        fav8.setOnClickListener {
            deleteFav(fav8)
        }
        fav9.setOnClickListener {
            deleteFav(fav9)
        }
        fav10.setOnClickListener {
            deleteFav(fav10)
        }
        returnBtn.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }


    private fun saveDeleted(textView: TextView){
        val sharedPreferences = getSharedPreferences("deleted", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("delete", textView.text.toString())

    }

    private fun readDeleted(textView: TextView){
        val sharedPreferences = getSharedPreferences("deleted", Context.MODE_PRIVATE)
        textView.text = sharedPreferences.getString("delete", "")

    }

    private fun noFavs(){
        if(fav1.text.isNotEmpty() or
            fav2.text.isNotEmpty() or
            fav3.text.isNotEmpty() or
            fav4.text.isNotEmpty() or
            fav5.text.isNotEmpty() or
            fav6.text.isNotEmpty() or
            fav7.text.isNotEmpty() or
            fav8.text.isNotEmpty() or
            fav9.text.isNotEmpty() or
            fav10.text.isNotEmpty()) {
            nothingTextView.text = ""
        }
//

    }


}


